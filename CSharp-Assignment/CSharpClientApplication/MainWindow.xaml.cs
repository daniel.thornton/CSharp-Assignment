﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Data;
using System.IO;
using System.Windows;

namespace CSharpClientApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />

    public partial class MainWindow : Window
    {
        /// <summary>
        /// Main Application window
        /// </summary>
        ///

        public ClientViewModel ViewModel = new ClientViewModel();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            DataTable Stocks;
            DataContext = ViewModel;
            WebServiceIntegration WSI = new WebServiceIntegration();
            string JsonStr = WSI.TriggerLDB();
            ViewModel.Hexvis = WSI.ShowError();

            try
            {
                //Due to inconsistent results read from dataset index[0] (of 1 table) to get the data-table.

                DataSet TmpSet = JsonConvert.DeserializeObject<DataSet>(JsonStr);
                Stocks = TmpSet.Tables[0];
            }
            catch (System.ArgumentException)
            {
                Stocks = new DataTable();
                Stocks.Columns.Add(new DataColumn("ERROR Contacting WebServer"));
            };
            ViewModel.StockInfo = Stocks;

            InitializeComponent();
        }

        private void SaveQuery_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Query"; // Default file name
            dlg.DefaultExt = ".Query"; // Default file extension
            dlg.Filter = "Query File (.QF)|*.Qf"; // Filter files by extension

            // Show save file dialog box
            var result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;

                var K = File.CreateText(filename);
                K.WriteLine(ViewModel.EndDate.ToShortDateString());
                K.WriteLine(ViewModel.StartDate.ToShortDateString());
                K.WriteLine(ViewModel.Enablefilter.ToString());
                K.WriteLine(System.DateTime.Now.ToString());
                K.Close();
            }
        }

        private void EnableDateFilter_Checked(object sender, RoutedEventArgs e)
        {
            if (ViewModel.Enablefilter)
            {
                Filterupdateaction();
            }
        }

        private void Filterupdateaction()
        {
            if (ViewModel.Enablefilter)
            {
                string startdate;
                startdate = ViewModel.StartDate.ToShortDateString();

                string enddate;
                try
                {
                    enddate = ViewModel.EndDate.ToShortDateString();

                    ViewModel.StockInfo.DefaultView.RowFilter = $"date >= #{startdate}# and date <= #{enddate}#";
                }
                catch
                {
                    MessageBox.Show("Invalid Date Format");
                }
            }
            else
            {
                ViewModel.StockInfo.DefaultView.RowFilter = string.Empty;
            }
        }

        private void DatePicker_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Filterupdateaction();
        }

        private void DatePicker_SelectedDateChanged_1(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Filterupdateaction();
        }

        private void LoadQuery_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = "Query"; // Default file name
            dlg.DefaultExt = ".Query"; // Default file extension
            dlg.Filter = "Query File (.QF)|*.Qf"; // Filter files by extension

            // Show save file dialog box
            var result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                string filename = dlg.FileName;
                var K = File.OpenText(filename);
                ViewModel.EndDate = DateTime.Parse(K.ReadLine());
                ViewModel.StartDate = DateTime.Parse(K.ReadLine());
                ViewModel.Enablefilter = K.ReadLine() == "True";
                Filterupdateaction();
            }
        }
    }
}