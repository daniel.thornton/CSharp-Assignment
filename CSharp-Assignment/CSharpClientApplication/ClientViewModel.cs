﻿using System;
using System.ComponentModel;
using System.Data;
using System.Runtime.CompilerServices;
using System.Windows;

namespace CSharpClientApplication
{
    /// <summary>
    /// The View model for the WPF Interface
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public class ClientViewModel : INotifyPropertyChanged
    {
        private DateTime _EndDate;
        private DateTime _StartDate;
        private Visibility _HexVis;

        /// <summary>
        ///stock information DataTable
        /// </summary>
        /// <value>
        /// The stock information.
        /// </value>
        public DataTable StockInfo { get; set; }

        /// <summary>
        ///  value indicating whether Date Filter in enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enablefilter; otherwise, <c>false</c>.
        /// </value>
        public bool Enablefilter { get; set; }

        /// <summary>
        /// the start date for the filter.
        /// </summary>
        /// <value>
        /// The start date .
        /// </value>
        public DateTime StartDate
        {
            get
            {
                return _StartDate;
            }
            set
            {
                OnPropertyChanged(nameof(StartDate));
                _StartDate = value;
            }
        }

        /// <summary>
        /// the end date for the filter.
        /// </summary>
        /// <value>
        /// The end date for the filter.
        /// </value>
        public DateTime EndDate
        {
            get
            {
                return _EndDate;
            }
            set
            {
                OnPropertyChanged(nameof(EndDate));
                _EndDate = value;
            }
        }

        /// <summary>
        /// The visibilty for the off-line notification.
        /// </summary>
        /// <value>
        /// The visibilty for the off-line notification.
        /// </value>
        public Visibility Hexvis
        {
            get
            {
                return _HexVis;
            }
            set
            {
                OnPropertyChanged(nameof(Hexvis));
                _HexVis = value;
            }
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="PropertyName">Name of the property.</param>
        protected void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}