﻿using System.IO;
using System.Windows;

namespace CSharpClientApplication
{
    /// <summary>
    /// Class That Handles all WebService Integration and Json Parsing.
    /// </summary>
    public class WebServiceIntegration
    {
        /// <summary>
        /// Updates the Local Json Serialized Database table
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">No Connection to server could be established</exception>
        public string TriggerLDB()
        {
            string TempPath = Path.GetTempPath();
            //Build Path to %temp% Directory For local Db Storage
            TempPath = Path.Combine(TempPath, @"TempDB.Json");

            //Try to read DataBase Through Web Service
            try
            {
                var TableStream = new localhost.Service1();
                string JsonString = TableStream.GetTable();
                //Write New Json Stream over the Existing LDB
                JsonString = "";
                if (!string.IsNullOrEmpty(JsonString))
                {
                    File.WriteAllText(TempPath, JsonString);
                }
                else
                //FallBack to Pre Downloaded Data
                     if (File.Exists(TempPath))
                {
                    return File.ReadAllText(TempPath);
                }
                else
                {
                    //If No Connection and No Existing LDB throw new exception.
                    throw new System.ArgumentException("No Connection to server could be established");
                }

                return JsonString;
            }
            catch
            {
                //If No Connection and No Existing LDB throw new exception.
                throw new System.ArgumentException("No Connection to server could be established");
            }
        }

        internal Visibility ShowError()
        {
            try
            {
                localhost.Service1 X = new localhost.Service1();
                X.GetTable();
                return Visibility.Hidden;
            }
            catch
            {
                return Visibility.Visible;
            }
        }
    }
}