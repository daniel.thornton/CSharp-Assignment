﻿using System;
using System.ServiceModel;

namespace Csharp_WebService
{
    [ServiceContract]
    public interface IService1
    {
        /// <summary>
        /// String that Returns a Json Serialized String Containing a Datatable of all information available in the Sql Table. Warning That will return a Very long String.
        /// </summary>
        /// <returns>Json Serialized string containing a Sql Table from the Main DataBase</returns>
        [OperationContract]
        [ServiceKnownType(typeof(DBNull))]
        string GetTable();
    }
}