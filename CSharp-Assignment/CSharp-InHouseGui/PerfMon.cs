﻿using System;
using System.IO;
using System.Management;
using System.Windows;

namespace CSharp_InHouseGui
{
    /// <summary>
    /// This Class Handles all Performance Monitoring For the Cpu and HDD Pie Charts
    /// </summary>
    public class PerfMon
    {
        /// <summary>
        /// Perfs the counter.
        /// </summary>
        /// <param name="Type">Type of Counter 1 for Cpu 2 for HDD</param>
        /// <returns>
        /// Returns a Double containing either cpu or HDD Percentage
        /// </returns>
        internal static double PerfCounter(int Type)
        {
            ConnectionOptions Options = new ConnectionOptions();

            //Note you will need to change this to the FQDN of the Server To release this from the sandbox environment
            ManagementScope CompManagementScope = new ManagementScope("\\\\Notchos\\root\\cimv2", Options);
            try
            {
                //open Connection to the Server
                CompManagementScope.Connect();
                ObjectQuery Query;
                ManagementObjectSearcher Searcher;
                ManagementObjectCollection QueryCollection;

                //If Cpu Type Called
                if (Type == 1)
                {
                    Query = new ObjectQuery("SELECT * FROM Win32_Processor");
                    Searcher = new ManagementObjectSearcher(CompManagementScope, Query);
                    QueryCollection = Searcher.Get();
                    ushort X = 0;

                    foreach (ManagementBaseObject O in QueryCollection)
                    {
                        X = (ushort)O["loadpercentage"];
                    }

                    return X;
                }
                else if (Type == 2)
                {
                    Query = new ObjectQuery("SELECT * FROM CIM_LogicalDisk");
                    Searcher = new ManagementObjectSearcher(CompManagementScope, Query);
                    QueryCollection = Searcher.Get();
                    double Currentv = 0;
                    double MaximumV = 0;
                    foreach (ManagementBaseObject O in QueryCollection)
                    {
                        ManagementObject N = (ManagementObject)O;

                        if (N["deviceid"].ToString() != "C:") continue;

                        if (!ReferenceEquals(N["freespace"], null))
                        {
                            Currentv = (ulong)N["freespace"];
                        }

                        if (!ReferenceEquals(N["size"], null))
                        {
                            MaximumV = (ulong)N["size"];
                        }
                    }

                    Currentv = Currentv / 1073741824;
                    //Convert bytes to GB
                    MaximumV = MaximumV / 1073741824;
                    //Convert bytes to GB

                    return Currentv / MaximumV * 100;
                }
                else
                {
                    Query = new ObjectQuery("SELECT * FROM Win32_OperatingSystem");
                    ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
                    QueryCollection = searcher.Get();
                    double CurrentV = 0;
                    double MaximumV = 0;

                    foreach (ManagementObject O in QueryCollection)
                    {
                        MaximumV = (ulong)O["TotalVisibleMemorySize"];
                        CurrentV = (ulong)O["FreePhysicalMemory"];
                    }

                    CurrentV = CurrentV / (1024 * 1024);
                    //Convert bytes to GB
                    MaximumV = MaximumV / (1024 * 1024);
                    //Convert bytes to GB

                    return CurrentV / MaximumV * 100;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("connection to the server Could Not be established");
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt", "Error Connecting to Wmic server Error:" + e.Message + "\n" + DateTime.Now.ToLongTimeString());

                return 1;
            }
        }
    }
}
