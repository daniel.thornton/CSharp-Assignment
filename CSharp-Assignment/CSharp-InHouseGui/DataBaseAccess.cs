﻿using CSharp_InHouseGui.NYSEDataTableAdapters;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows;

namespace CSharp_InHouseGui
{
    /// <summary>
    /// Class that Allows access to database.
    ///  </summary>
    public class DataBaseAccess
    {
        /// <summary>
        /// Returns All Values from DB to fill data-grid
        /// </summary>
        /// <returns>
        /// Return DataGrid of Table information
        /// </returns>
        public static DataTable FillGrid()
        {
            NYSEDailyPriceTableAdapter NyseAdapter = new NYSEDailyPriceTableAdapter();
            try {
                DataTable Grid = NyseAdapter.GetData();
                return Grid;
            }
            catch (Exception e)
            {
                MessageBox.Show("connection to the server Could Not be established");
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt", "Error Connecting to Server Error:" + e.Message +"\n" + DateTime.Now.ToLongTimeString());

                return new DataTable();
            }
        }

        /// <summary>
        /// Returns a DataTable of Data in the table Filtered by the Symbol Selected
        /// </summary>
        /// <param name="SymbolSelected">String of the Currently selected Stick Symbol</param>
        /// <returns>DataTable Containing a Filtered Table</returns>

        public static DataTable FillGridBySymbol(string SymbolSelected)
        {
            try {
                NYSEDailyPriceTableAdapter NyseDailyPriceTableAdapter = new NYSEDailyPriceTableAdapter();
                DataTable X = NyseDailyPriceTableAdapter.GetDataBySymbol(SymbolSelected);
                return X;
            }
            catch (Exception e)
            {
                File.AppendAllText(System.Reflection.Assembly.GetExecutingAssembly().Location + "errorlog.txt", "Error Connecting to Server Error:" + e.Message + "\n" + DateTime.Now.ToLongTimeString());

                return new DataTable();
            }
        }

        /// <summary>
        /// Fills the grid by a specified date range
        /// </summary>
        /// <param name="StartDate">DateTime initial date</param>
        /// <param name="EndDate">DateTime EndDate</param>
        /// <returns>A DataTable Filtered by Date</returns>

        public static DataTable FillGridByDate(DateTime StartDate, DateTime EndDate)

        {
            try {
                NYSEDailyPriceTableAdapter NyseAdapter = new NYSEDailyPriceTableAdapter();
                DataTable Grid = NyseAdapter.GetDataByDate(StartDate.ToString(), EndDate.ToString());
                return Grid;
            }
            catch (Exception e)
            {
                File.AppendAllText(System.Reflection.Assembly.GetExecutingAssembly().Location + "errorlog.txt", "Error Connecting to Server Error:" + e.Message + "\r\n" + DateTime.Now.ToLongTimeString());

                return new DataTable();
            }
        }

        /// <summary>
        /// Fills a DataTable By Selected Startdate,EndDate and Selected Symbol
        /// </summary>
        /// <param name="StartDate">DateTime Initial Date</param>
        /// <param name="EndDate">DateTime End Date</param>
        /// <param name="SelectedSymbol">String SymbolName to Filter by</param>
        /// <returns>A DataTable Filtered by date</returns>

        public static DataTable FillGridByDate(DateTime StartDate, DateTime EndDate, string SelectedSymbol)
        {
            NYSEDailyPriceTableAdapter NyseAdapter = new NYSEDailyPriceTableAdapter();
            NYSEData.NYSEDailyPriceDataTable X = NyseAdapter.GetDataByDateAndCol(StartDate.ToString(), EndDate.ToString(), SelectedSymbol);
            return X;
        }

        /// <summary>
        /// Calls to the database and gets unique Symbols and their counts
        /// </summary>
        /// <returns>
        /// Returns a datatable containing two columns. Count(int) Containing the count of unique symbols, Symbol_Name(String) Name of symbol
        /// </returns>

        public static DataTable SymbolListTable()
        {
            try {
                NYSEDailyPriceSymbolAdaptorTableAdapter NyseAdapter = new NYSEDailyPriceSymbolAdaptorTableAdapter();
                NYSEData.NYSEDailyPriceSymbolAdaptorDataTable X = NyseAdapter.GetData();
                return X; }
            catch (Exception e)
            {
                File.AppendAllText(System.Reflection.Assembly.GetExecutingAssembly().Location + "errorlog.txt", "Error Connecting to Server Error:" + e.Message + "\n" + DateTime.Now.ToLongTimeString());

                return new DataTable();
            }
        }

        /// <summary>
        /// Calls to the dateBase and gets Date Filtered unique symbols and their counts
        /// </summary>
        /// <param name="StartDate">DateTime The initial date to filter by</param>
        /// <param name="EndDate">DateTime the end date to filter by</param>
        /// <returns>
        /// Returns a datatable containing two columns. Count(int) Containing the count of unique symbols, Symbol_Name(String) Name of symbol
        /// </returns>

        public static DataTable SymbolListTable(DateTime StartDate, DateTime EndDate)
        {
            NYSEDailyPriceSymbolAdaptorTableAdapter NyseAdapter = new NYSEDailyPriceSymbolAdaptorTableAdapter();
            NYSEData.NYSEDailyPriceSymbolAdaptorDataTable X = NyseAdapter.GetDataByDate(StartDate.ToString(), EndDate.ToString());
            return X;
        }

        /// <summary>
        /// Commits the changes to the data table and Parses them to the Sql server.
        /// </summary>
        /// <param name="Changes">The changes made to the dataset.</param>
        public static void CommitChanges(DataTable Changes)
        {
            foreach (DataRow Row in Changes.Rows)
            {
                try
                {
                    string connectionString =
                       @"Server = NOTCHOS\CSHARPASSESMENT; Database = NYSEDailyPrices; Trusted_Connection = True";
                    using (SqlConnection SqlCon =
                        new SqlConnection(connectionString))
                    {
                        SqlCon.Open();
                        using (SqlCommand cmd = new SqlCommand(

                            "UPDATE NYSEDailyPrice SET " +
                            "exchange = @exchange, " +
                            "stock_symbol = @stock_symbol, " +
                            "date = @date, " +
                            "stock_price_open = @stock_price_open, " +
                            "stock_price_high = @stock_price_high, " +
                            "stock_price_low = @stock_price_low, " +
                            "stock_price_close = @stock_price_close, " +
                            "stock_volume = @stock_volume, " +
                            "stock_price_adj_close = @stock_price_adj_close " +
                            " WHERE date=@Date and Stock_Symbol = @Stock_symbol ", SqlCon
                            )
                          )
                        {
                            cmd.Parameters.AddWithValue("exchange", Row[0]);
                            cmd.Parameters.AddWithValue("stock_symbol", Row[1]);
                            cmd.Parameters.AddWithValue("date", Row[2]);
                            cmd.Parameters.AddWithValue("stock_price_open", Row[3]);
                            cmd.Parameters.AddWithValue("stock_price_high", Row[4]);
                            cmd.Parameters.AddWithValue("stock_price_low", Row[5]);
                            cmd.Parameters.AddWithValue("stock_price_close", Row[6]);
                            cmd.Parameters.AddWithValue("stock_volume", Row[7]);
                            cmd.Parameters.AddWithValue("stock_price_adj_close", Row[8]);

                            cmd.ExecuteNonQuery();

                            //rows number of record got updated
                        }
                    }
                }
                catch (SqlException ex)
                {
                    if (ex.Message.IndexOf("NYSEDailyPrices") < 0)
                    {
                        MessageBox.Show("DateBase Not Found please ensure the table NYSEDailyPrices exists", "dataBase Connection Error");
                    }
                }
            }
        }
    }
}