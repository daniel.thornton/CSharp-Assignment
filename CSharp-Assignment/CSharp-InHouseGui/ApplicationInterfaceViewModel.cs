﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Runtime.CompilerServices;

namespace CSharp_InHouseGui
{
    /// <summary>
    /// View model for the WPF Front End
    /// </summary>
    /// <remarks>
    /// This is used to initiate and update all Front end GUI Controls. <para>Note When Changing in here ensure to Trigger an on property changed.</para>
    /// </remarks>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />

    public class ApplicationInterfaceViewModel : INotifyPropertyChanged
    {
        private double _cpuPer = PerfMon.PerfCounter(1);
        private double _hddFree = PerfMon.PerfCounter(2);
        private string _cpuString;
        private string _hddString;
        private double _hddPer;
        private double _cpuPer1;
        private DateTime _startDate;
        private DateTime _endDate;
        private bool _enableDateFilter;
        private DataTable _sqlDataTable;
        private bool _enableSymbolFilter;
        private List<StockSymbol> _symbolList;
        private StockSymbol _selectedSymbol;
        private string _ramString;
        private double _RamPer;

        /// <summary>
        /// Gets or sets the ram string.
        /// </summary>
        /// <value>
        /// The ram string.
        /// </value>
        public string RamString
        {
            get
            {
                return _ramString;
            }
            set
            {
                _ramString = value;
                OnPropertyChanged(nameof(RamString));
            }
        }

        /// <summary>
        /// String to show below the CPU Pie Chart
        /// </summary>
        /// <value>
        /// The cpu string.
        /// </value>

        public string CpuString
        {
            get { return _cpuString; }
            set
            {
                _cpuString = value;
                OnPropertyChanged(nameof(CpuString));
            }
        }

        /// <summary>
        /// String to show below the HDD Pie Chart
        /// </summary>
        /// <value>
        /// The HDD string shown below the Pie.
        /// </value>

        public string HddString
        {
            get { return _hddString; }
            set
            {
                if (value == HddString) return;
                _hddString = value;
                OnPropertyChanged(nameof(HddString));
            }
        }

        /// <summary>
        /// Double for populating the percentage of the HDD pie chart
        /// </summary>
        /// <value>
        /// The HDD percentage Tells the pie what to show.
        /// </value>

        public double HddPer
        {
            get { return _hddPer; }
            set
            {
                _hddPer = value;
                OnPropertyChanged(nameof(HddPer));
            }
        }

        /// <summary>
        /// Gets or sets the ram per.
        /// </summary>
        /// <value>
        /// The ram per.
        /// </value>
        public double RamPer
        {
            get { return _RamPer; }
            set
            {
                _RamPer = value;
                OnPropertyChanged(nameof(RamPer));
            }
        }

        /// <summary>
        /// Var that enables or disables the date filters
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable date filter]; otherwise, <c>false</c>.
        /// </value>

        public bool EnableDateFilter
        {
            get { return _enableDateFilter; }
            set { _enableDateFilter = value; OnPropertyChanged(nameof(EnableDateFilter)); }
        }

        /// <summary>
        /// The Initial date to filter between
        /// </summary>
        /// <value>
        /// The start date of the Date Filter.
        /// </value>
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                OnPropertyChanged(nameof(StartDate));
            }
        }

        /// <summary>
        /// The End date of the Date filter
        /// </summary>
        /// <value>
        /// The end date of the date Filter.
        /// </value>
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                OnPropertyChanged(nameof(EndDate));
            }
        }

        /// <summary>
        /// Double For populating the percentage of the CPU pie chart
        /// </summary>
        /// <value>
        /// The cpu percentage to populate the CPU Pie.
        /// </value>

        public double CpuPer
        {
            get { return _cpuPer1; }
            set { _cpuPer1 = value; OnPropertyChanged(nameof(CpuPer)); }
        }

        /// <summary>
        /// DataTable used to Populate the DataGrid.
        /// Will Initiate a PropertyChanged Handler
        /// </summary>
        /// <value>
        /// The SQL data table containing all required information to populate the DataGrid.
        /// </value>

        public DataTable SqlDataTable
        {
            get { return _sqlDataTable; }
            set
            {
                _sqlDataTable = value;
                OnPropertyChanged(nameof(SqlDataTable));
            }
        }

        /// <summary>
        /// Boolean Value to Enable or disable the Symbol Filter
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable symbol filter]; otherwise, <c>false</c>.
        /// </value>

        public bool EnableSymbolFilter
        {
            get { return _enableSymbolFilter; }
            set { _enableSymbolFilter = value; OnPropertyChanged(nameof(EnableSymbolFilter)); }
        }

        /// <summary>
        /// List of Stock Symbols in the dataTable
        /// </summary>
        /// <value>
        /// The symbol list Contains all symbols in the table.
        /// </value>
        /// <seealso cref="StockSymbol" />
        public List<StockSymbol> SymbolList
        {
            get { return _symbolList; }
            set { _symbolList = value; OnPropertyChanged(nameof(SymbolList)); }
        }

        /// <summary>
        /// The users Currently selected dataSymbol
        /// </summary>
        /// <value>
        /// The user selected symbol.
        /// </value>
        public StockSymbol SelectedSymbol
        {
            get { return _selectedSymbol; }
            set { _selectedSymbol = value; OnPropertyChanged(nameof(SelectedSymbol)); }
        }

        /// <summary>
        /// Updates Variables for the performance counter
        /// </summary>
        /// <param name="UpdateVarIndex">Int UpdateVarIndex: 1 to update CPU 2 to update Hdd Stats</param>
        /// <seealso cref="PerfMon.PerfCounter(int)" />

        public void UpdateVar(int UpdateVarIndex)
        {
            switch (UpdateVarIndex)
            {
                case 1:
                    _cpuPer = PerfMon.PerfCounter(1);
                    CpuPer = (360 - (100D - _cpuPer) / 100) * 360;
                    CpuString = @"Cpu Utilization " + (_cpuPer).ToString() + "%";
                    break;

                case 2:
                    _hddFree = PerfMon.PerfCounter(2);
                    HddPer = (360 - ((_hddFree / 100) * 360));
                    HddString = @"freeSpace: " + Math.Round(_hddFree, 2) + "Gb";
                    break;

                case 3:
                    _RamPer = PerfMon.PerfCounter(3);
                    RamString = @"Ram Utilization " + (100D - Math.Round(_RamPer, 2)) + "%";
                    RamPer = 360 - ((_RamPer / 100) * 360);
                    break;
            }
        }

        /// <summary>
        /// PropertyChanged Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="PropertyName">Name of the property.</param>
        protected void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }

    /// <summary>
    /// A Type For storing Stock Symbols Hold the symbol and the count of occurrences of the symbol
    /// </summary>
    public class StockSymbol

    {
        /// <summary>
        /// String containing the name of the Stock
        /// </summary>
        /// <value>
        /// The Stock symbol.
        /// </value>
        public string Symbol { get; set; }

        /// <summary>
        /// Number of times the symbol appears in the table
        /// </summary>
        /// <value>
        /// The count of the symbol.
        /// </value>
        public double SymbolCount { get; set; }
    }
}