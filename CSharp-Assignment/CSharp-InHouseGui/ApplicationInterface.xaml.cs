﻿using System.Collections.Generic;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace CSharp_InHouseGui
{
    /// <summary>
    /// Interaction logic for ApplicationInterface.xaml
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class ApplicationInterface
    {
        /// <summary>
        ///
        /// The Application ViewModel
        /// </summary>

        //Declare initial View model
        public ApplicationInterfaceViewModel Vm = new ApplicationInterfaceViewModel();

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationInterface"/> class.
        /// </summary>
        /// <inheritdoc />
        public ApplicationInterface()
        {
            InitializeComponent();

            //Update initial Vales for performance counter
            Vm.UpdateVar(1);
            Vm.UpdateVar(2);
            Vm.UpdateVar(3);

            //Populate the list of symbols
            Vm.SymbolList = new List<StockSymbol>();

            //Fill the DataGrid with All Available information in Sql Table
            Vm.SqlDataTable = DataBaseAccess.FillGrid();
            UpdateSymbolList(1);

            //Set Item Source for the Stock list

            StockList.ItemsSource = Vm.SymbolList;

            //Set the data context to my application view model.
            DataContext = Vm;
        }

        /// <summary>
        /// Handles the MouseDown event of the HddPie control.
        /// </summary>
        /// <param name="Sender">Clicking on the HDD Pie Triggers this.</param>
        /// <param name="E">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void HddPie_MouseDown(object Sender, System.Windows.Input.MouseButtonEventArgs E)
        {
            // Update HDD Perf Counter
            Vm.UpdateVar(2);
        }

        /// <summary>
        /// Handles the MouseDown event of the CPUPie control.
        /// </summary>
        /// <param name="Sender">The source of the event.</param>
        /// <param name="E">The <see cref="System.Windows.Input.MouseButtonEventArgs" /> instance containing the event data.</param>
        private void CpuPie_MouseDown(object Sender, System.Windows.Input.MouseButtonEventArgs E)
        {
            //Update Cpu Perf Counter
            Vm.UpdateVar(1);
        }

        /// <summary>
        /// Handles the Checked event of the EnableDateFilter control.
        /// </summary>
        /// <param name="Sender">Triggered on clicking the enable date filter Edit box</param>
        /// <param name="E">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>

        private void EnableDateFilter_Checked(object Sender, RoutedEventArgs E)
        {
            //Update Grid to reflect Filtered Data
            DateFilterAction();
        }

        /// <summary>
        /// Handles the OnSelectedDateChanged event of the DatePicker control.
        /// </summary>
        /// <param name="Sender">Triggered when the DateChanged Checkbox is edited</param>
        /// <param name="E">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void DatePicker_OnSelectedDateChanged(object Sender, SelectionChangedEventArgs E)
        {
            //Update Grid to Reflect Filtered Content
            DateFilterAction();
        }

        /// <summary>
        /// Handles the Checked event of the EnableSymbolFilter control.
        /// </summary>
        /// <param name="Sender">Triggered when the user changes the Symbol Filter</param>
        /// <param name="E">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void EnableSymbolFilter_Checked(object Sender, RoutedEventArgs E)
        {
            //Update Grid to reflect the Filtered Content
            DateFilterAction();
        }

        /// <summary>
        /// Updates the stock list.
        /// </summary>
        private void UpdateSymbolList(int Init)
        {
            //Loop through Rows in the Symbol list table to populate the Stock list
            foreach (DataRow X in DataBaseAccess.SymbolListTable().Rows)
            {
                StockSymbol KStockSymbol = new StockSymbol
                {
                    Symbol = (string)X["stockSymbols"],
                    SymbolCount = (int)X["Count"]
                };

                Vm.SymbolList.Add(KStockSymbol);
            }
        }

        private void UpdateSymbolList()
        {
            //Loop through Rows in the Symbol list table to populate the Stock list
            foreach (DataRow X in DataBaseAccess.SymbolListTable(Vm.StartDate, Vm.EndDate).Rows)
            {
                StockSymbol KStockSymbol = new StockSymbol
                {
                    Symbol = (string)X["stockSymbols"],
                    SymbolCount = (int)X["Count"]
                };

                Vm.SymbolList.Add(KStockSymbol);
            }
        }

        /// <summary>
        /// Runs when the Date filter is Changed, Updates the DataGrid to Contain Filtered information.
        /// </summary>
        private void DateFilterAction()

        {
            if (Vm.SelectedSymbol != null && Vm.EnableDateFilter && Vm.EnableSymbolFilter)
            {
                //Use Appropriate filter for the Grid
                Vm.SqlDataTable = DataBaseAccess.FillGridByDate(Vm.StartDate, Vm.EndDate, Vm.SelectedSymbol.Symbol);
                Vm.SymbolList.Clear();
                UpdateSymbolList();
            }
            else if (Vm.EnableDateFilter)

            {
                Vm.SqlDataTable = DataBaseAccess.FillGridByDate(Vm.StartDate, Vm.EndDate);
                Vm.SymbolList.Clear();
                UpdateSymbolList();
            }
            else if (Vm.EnableSymbolFilter && Vm.SelectedSymbol.Symbol != null)
            {
                Vm.SqlDataTable = DataBaseAccess.FillGridBySymbol(Vm.SelectedSymbol.Symbol);
            }
        }

        /// <summary>
        /// Handles the OnSelectionChanged event of the StockList control.
        /// </summary>
        /// <param name="Sender">Triggered when the user Changed what stock is selected.</param>
        /// <param name="E">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void StockList_OnSelectionChanged(object Sender, SelectionChangedEventArgs E)
        {
            DateFilterAction();
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            Vm.SqlDataTable = DataBaseAccess.FillGrid();
            Vm.EnableDateFilter = false;
            Vm.EnableSymbolFilter = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(@"Are you sure?", @"Are you sure you want to Commit your changes?", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes && Vm.SqlDataTable.GetChanges() != null)
            {
                DataBaseAccess.CommitChanges(Vm.SqlDataTable.GetChanges());
            }
        }

        private void RamPie_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Vm.UpdateVar(3);
        }
    }
}